import re
import json
import pandas as pd
import numpy as np

def create_match_info(student_response, match, concept, match_type, weight):
    return {
        'student_concept_evidence' : student_response[match.start():match.end()],
        'concept' : concept,
        'type' : match_type,
        'weight' : weight
    }

def analyze(student_response, full_credit_regexes, partial_credit_regexes, pcw=0.5):
    n = len(full_credit_regexes)
    matches = []
    hits = 0
    concepts_hit = {}
    
    for key in list(full_credit_regexes.keys()):
        concepts_hit[key] = 0
    
    for concept_name, concept_regexes in full_credit_regexes.items():
        for regex in concept_regexes:
            q = re.search(regex, student_response)
            if q:
                match_info = create_match_info(student_response, q, concept_name, 'full_credit', 1/n)
                matches.append(match_info)
                concepts_hit[concept_name] = 1
                hits += 1
                break
                
    score = (hits / n)
    if score < 1.0 and len(partial_credit_regexes) > 0:
        for concept_name, hit in concepts_hit.items():
            if hit > 0:
                continue
            if concept_name in partial_credit_regexes:
                concept_regexes = partial_credit_regexes[concept_name]
                for regex in concept_regexes:
                    q = re.search(regex, student_response)
                    if q:
                        match_info = create_match_info(student_response, q, concept_name, 'partial_credit', pcw /n )
                        matches.append(match_info)
                        hits += pcw
                        break

        # (possibly) update score and return
        return (hits / n, matches)
    else:
        return (score, matches)
    
    
def read_df(filepath):
    return pd.read_csv(filepath, sep='~', index_col=False)

def get_regex_from_table(df, aid, qid):
    full_credit = df[np.logical_and(df['AID'] == aid, df['QID'] == qid)]['Full Credit Matches'].iloc[0].replace("\\", "\\\\")
    full_credit_re = json.loads(full_credit)
    partial_credit = df[np.logical_and(df['AID'] == aid, df['QID'] == qid)]['Partial Credit Matches'].iloc[0].replace("\\", "\\\\")
    partial_credit_re = json.loads(partial_credit)
    
    return full_credit_re, partial_credit_re
    
    
def evaluate_regex(df, user_df, debug=False, thresh=1.0):
    
    actual_scores = df['Score'].as_matrix()
    pred_scores = []
    raw_scores = []
    match_info = []
    missed_items = []
    hit_items = []
    
    for index, row in df.iterrows():
        aid = row['AID']
        qid = row['QID']
        
        if debug:
            print('Assignment/Question/SID: [{}|{}|{}]'.format(aid, qid, row['SID']))
        
        full_credit_re, partial_credit_re = get_regex_from_table(user_df, aid, qid)
        
        student_response = row['Student Answer']
        
        score, matches = analyze(student_response.lower(), full_credit_re, partial_credit_re)
        if np.abs(score - actual_scores[index]) > thresh:
            missed_items.append(index)
        elif np.abs(score - actual_scores[index]) <= thresh:
            hit_items.append(index)
        match_info.append(matches)
        
        # scale the score
        scaled_score = np.round(score * 5)
        raw_scores.append(score)
        pred_scores.append(scaled_score)
    
    
    return {
        'y_true' : np.round(actual_scores), 
        'y_pred' : np.array(pred_scores),
        'y_raw_true' : actual_scores,
        'y_raw_pred' : np.array(raw_scores), 
        'match_info' : np.array(match_info),
        'missed_items' : missed_items,
        'hit_items' : hit_items
    }