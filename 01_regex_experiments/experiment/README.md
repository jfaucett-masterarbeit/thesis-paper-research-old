## README

This directory contains one file `asag_regex_example.csv`.

That file contains a list of teacher prompts and model answers for short answer questions for three assignments.

## DESCRIPTION OF DATA

1. **AID** : Assignment ID
2. **QID** : Question ID
3. **Prompt** : The prompt which the student sees
4. **Model Answer** : The perfect answer which the teacher has already written.
5. **Full Credit Matches** : a JSON data structure containing the concepts and a list of regular expressions which match these concepts for full credit.
6. **Partial Credit Matches** : a JSON data structure containing concepts and a list of regular expressions which match these concepts giving the student partial credit.

Please delete the first entries in Full Credit and Partial Credit Matches and write your own regular expressions.
The first entry has been done so that you can see what format all the other entries should have.

Below is a description of the format:

```javascript
{
    "name_of_function" : [
        "names? of (function|method)s?",
        "(function|method)s names?",
    ],
    "types_of_parameters" : [
        "types? of(.*)(parameter|argument)s?",
    ]
}
```

Here **name_of_function** is the label for the concept. There can be any number of concepts but usually there is between 1 and 3-4.

The **"names? of (function|method)s?"** entry is a regular expression. These are used to match against the student's responses and check whether the student answered correctly or not.

### INSTRUCTIONS

1. Follow this tutorial to learn about regular expressions: [regexone.com](https://regexone.com).
2. When you are done with the tutorial in step one, start a Timer.
3. Open up the CSV file in your editor Fill in all the entries in the CSV File for the fields "Full Credit Matches" and "Partial Credit Matches". "Partial Credit Matches" are not always required, if you can't think of anything for partial credit that's fine, just try to fill it in when you can.
4. When you are done Save your file and stop the Timer.
5. Send me the CSV file
