Benotung: 
1. 5-Klassen indentifiezier.


Observed Situations:
1. A student matches the model response and the concepts perfectly = 5.0, Label = ```correct_complete```
2. A student matches all the concepts but words it somewhat differently = 5.0, Label = ```correct_indirect```
3. A student has no answer/ answer unrelated to question = 0.0 ```wrong_unrelated```
4. A student has an answer that is related to the question but it is wrong = 0.0, Label = ```wrong_related```.
4. A student gets all/most concepts fully except for one of them which he kind of hints at = 4, ```partially_correct```
5. A student answers one or more concepts correctly but completely misses another one = 2-4, ```paratially_correct```
6. A student writes a lot about things that are correct but not really related to question = 0-2, ```wrong_unrelated```

Notes:
1. Almost impossible to be consistent even in the same problem set.
2. Time consuming task.
3. Students are usually correct in very similar ways.
4. Partially correct answers seem more diverse. (range from 2 - 4) : Some overlap with correct_indirect
5. Incorrect/Wrong unrelated students and usually uniquely wrong.
6. Wrong related responses seem uniquely wrong as well.


For ```Partially_Correct```:
1. Option 1: when SA does not contain a concept.
2. Option 2: when SA contains one or some of the concepts but additional wrong/contradictory information.
3. Option 3: when SA contains all concepts but additional wrong/contradictory information.


Stages:
1. After grading 146 Problems: r = 0.75