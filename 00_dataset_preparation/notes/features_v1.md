## Feature 1: Length Difference

1. When Model_Answer <  Student_Answer, then the Score tends to be slightly higher [54%]: (4.05 +/- 1.05)
2. When Model_Answer > Student_Answer, then Score tends to be lower [38%]: (3.80 +/- 1.33)

Description:

The difference (|MA| - |SA|) has a negative correlation to the score. 
As difference increases positively (MA > SA), score tends to decrease. 

This makes sense, take the following pair for the prompt: "What makes up a function signature?".

1. Model Answer: "The name of the function and the types of its parameters".
2. This will be tokenized to: [name,of,function,types,of,its,parameters].
3. If a student does not at minimum write: [name,function,types,parameters] or close synonyms thereof he will not get a high score. In this example that represents 4/7 or 0.571 of the model answer tokens. Statistically it is simply more likely that he will hit the correct tokens if he writes more words in his answer.

Exceptions:

It still depends greatly on the question. Some questions can be answered with one or a few words. 

For instance, "Where does a C program start?", Answer: "A program starts at the main function".
Student Answer: "main", or "at main" suffice.

## Feature 2: Term Count Vectors

### Distance Measures

1. **Cosine Similarity**: r = 0.45, high positive correlation with score. This makes sense b/c the cosine similarity does not look at the absolute difference b/t endpoints of a vector but at the angle b/t them. So if two vectors are pointing in the same direction they are similar regardless of the magnitiudes. The euclidean and manhatten distance performed poorly b/c they look more at absolute distances.

Example:

S1: "man jumped" => [man,jumped] => [1,1,0]
S2: "man ran" => [man,ran] => [1,0,1]
Dictionary: [man,jumped,ran]

Open Questions:

1. Why is jaccard almost is good as cosine? (r = 0.38 vs. r=0.45) 
2. Are there situations where jaccard is better than cosine? What are they?
3. Why is Jensen-Shannon Divergence better than Jaccard?


### Feature 3: N-Grams

From the Term Count exploration:

1. The pure 1-Gram Model (R = 0.46), adding Bigrams and Trigrams to this reduced the overall overlap of the models hence the correlation with the score.
2. However, pure Bigram (no unigrams or trigrams), and Trigram models correlated (0.31 and 0.19) respectively. This means finding matching pairs of Bigrams and Trigrams in the model answer and student answer does correlate with the final score.


### Ngram Matches

1. Unigram match correlates positively the highest with the score.
2. Bigram matches correlate high as well especially with a factor of 1/3 boosting. (instead of 1/2)
3. Trigrams correlate lowest but still a bit (r=0.37)


### Feature 4: Knowledge-Base and Corpus Based Word-to-Word Similarity Scores

1. Entailment Function - is there evidence in the student response for the contents of the model response?
2. Extract Information Score - How similar are the contents that are not in the model response to the model response?
  - This comes from the observation that incorrect responses are usually uniquely wrong, and correct responses tend to be more similar to the model response.
  
Shortest-Path:
1. Adding Lexical Matching had zero effect.
2. NOUN,VERB,ADJ, R = 0.458
3. NOUN,VERB,ADV, R = 0.428
4. NOUN,VERB,ADJ,ADV, R = 0.467

Others:
1. Leacock & Chodorow (Second Highest Correlation, r = 0.37)
2. Wu & Palmer (Lowest, r = 0.33)
3. Resnik (0.36)
4. Jiang & Conrath (Correlates highest with the score, r = 0.40)


### Feature 5: Vector Space Model Similarity Scores

1. WMD - correlates negatively with the score r = -0.29 (as the vectors move further apart the score drops)
2. VCOS - correlates positively (more strongly) with the score, r = 0.348 (should actually take 1 - this value, but basically means as the cosine similarit approaches 1 this correlatse positively with the score).

### Feature 6: Simple Token Measures

1. Edit Distance : Tokens, POS Tags, DepParse.
2. Jaccard, Dice, Cosine, Faucett

### Feature 7: Syntax

1. As the Edit Distance, POS Distance, DP Distance increase score drops (inverse relationship)
2. ED (r=-0.35), POS (r = -0.23), DP (r= -0.16)
3. Tree Edit Distance w/Lemmas: (r= -0.07)
4. Bleu Score: (r= 0.21)

