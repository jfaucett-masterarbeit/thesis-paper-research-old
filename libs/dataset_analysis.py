# filename: dataset.py
# description: Visualization and analysis functions for a dataset.
# author: John Faucett

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np


def plot_responses_per_assignment_question(df):
    assignment_questions = df.groupby(['AID', 'QID'])[['Student Answer']].count()[
        'Student Answer'].as_matrix()
    plt.scatter(np.arange(len(assignment_questions)),
                assignment_questions, alpha=0.8)
    print('Responses per Assignment-Question (mean/std): {} +/-{:.2f}'.format(
        assignment_questions.mean(), assignment_questions.std()))
    tmp = df.groupby(['AID', 'QID'])['Student Answer'].count()
    print(tmp)


def print_assignment_and_question_mean_and_std_for_scores(df, ylim=[3, 5], xlim=[0, 21]):
    result = df.groupby(['AID', 'QID']).describe()

    x_labels = []
    y_mean = []
    y_std = []

    for group in result['Score'].sort_values(by=['mean'], ascending=False).iterrows():
        label = '{}.{}'.format(group[0][0], group[0][1])
        x_labels.append(label)
        y_mean.append(group[1]['mean'])
        y_std.append(group[1]['std'])

        # print('{}.{}: {:.2f} +/- {:.2f}'.format(group[0][0],
        #                                       group[0][1], group[1]['mean'], group[1]['std']))

    plt.figure(figsize=(16, 12))
    plt.title('Sorted by Mean')
    x_ticks = np.arange(len(x_labels)) + 1
    plt.errorbar(x_ticks, y_mean, y_std,
                 linestyle='None', marker='^', capsize=3)
    plt.xticks(x_ticks, x_labels, fontsize=12)
    plt.ylim(ylim)
    plt.xlim(xlim)
    plt.show()

    x_labels_std = []
    y_mean_std = []
    y_std_std = []

    for group in result['Score'].sort_values(by=['std'], ascending=False).iterrows():
        label = '{}.{}'.format(group[0][0], group[0][1])
        x_labels_std.append(label)
        y_mean_std.append(group[1]['mean'])
        y_std_std.append(group[1]['std'])

    plt.figure(figsize=(16, 12))
    plt.title('Sorted by Std.Deviation')
    x2_ticks = np.arange(len(x_labels_std)) + 1
    plt.errorbar(x_ticks, y_mean_std, y_std_std,
                 linestyle='None', marker='^', capsize=3)
    plt.xticks(x2_ticks, x_labels_std, fontsize=12)
    plt.ylim(ylim)
    plt.xlim(xlim)
    plt.show()


def plot_assignment_and_question_boxplot(df):

    grouped = df.groupby(['AID', 'QID'])
    df2 = pd.DataFrame({col: vals['Score'] for col, vals in grouped})
    meds = df2.mean().sort_values(ascending=True)
    df2 = df2[meds.index]
    bp = df2.boxplot(figsize=(16, 12), grid=False,
                     patch_artist=True, capprops={'color': 'red'}, boxprops={'hatch': '//'})
    plt.show()


def plot_assignment_and_question_histograms(df, rows=3, cols=7):
    plt.figure(figsize=(18, 14))
    idx = 1
    for group in df.groupby(['AID', 'QID']):
        # plot setup
        plt.subplot(rows, cols, idx)
        plt.subplots_adjust(hspace=0.5, wspace=0.5)

        # label
        aid = group[1]['AID'].iloc[0]
        qid = group[1]['QID'].iloc[0]
        aqid = '{}.{}'.format(aid, qid)

        scores = group[1]['Score'].as_matrix()

        # render histogram
        plt.title(aqid)
        plt.hist(scores, color='lightblue')
        plt.axvline(scores.mean(), color='r', linestyle='dashed', linewidth=1)
        plt.ylim([0, 30])
        plt.xlim([0, 5])

        idx += 1
    plt.show()
