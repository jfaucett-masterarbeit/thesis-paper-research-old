import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from sklearn.manifold import TSNE


def compute_tsne(data, perplexity=30, random_state=42):
    return TSNE(n_components=2, perplexity=perplexity, random_state=random_state).fit_transform(data)


def plot_tsne(x, scores, labels=[]):

    # We choose a color palette with seaborn.
    palette = np.array(sns.color_palette("hls", 8))

    plt.figure(figsize=(14, 12))
    plt.title("TSNE Scores for: {}".format(labels))
    i = 0
    for k in np.unique(scores):
        idx = scores == k
        plt.scatter(x[idx, 0], x[idx, 1], lw=0, s=50,
                    c=palette[i], alpha=0.8, label=k)
        i += 1
    plt.legend(loc='upper right')
    plt.axis('tight')
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.show()


def select_indexes_from_tsne(tsne_values, xbox, ybox):
    dim1 = np.logical_and(
        tsne_values[:, 0] >= xbox[0], tsne_values[:, 0] < xbox[1])
    dim2 = np.logical_and(
        tsne_values[:, 1] >= ybox[0], tsne_values[:, 1] < ybox[1])
    return np.logical_and(dim1, dim2)
