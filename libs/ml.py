from zss import simple_distance, Node


def get_node_label(node):
    if node.pos_ == 'PRON':
        return node.text.lower()
    else:
        # check for negation
        if 'neg' in [t.dep_ for t in node.children]:
            return 'neg_{}'.format(node.lemma_)
        else:
            return node.lemma_


def to_tree(node):
    if node.n_lefts + node.n_rights > 0:
        label = get_node_label(node)
        zss_Node = Node(label)
        for child in node.children:
            zss_Node.addkid(to_tree(child))
        return zss_Node
    else:
        label = get_node_label(node)
        zss_Node = Node(label)
        return zss_Node


def get_root(sent):
    return [t for t in sent if t.dep_ == 'ROOT'][0]


def tree_distance(s1, s2):
    tree1 = to_tree(get_root(s1))
    tree2 = to_tree(get_root(s2))
    return simple_distance(tree1, tree2)
