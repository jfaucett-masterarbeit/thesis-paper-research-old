import os
import pandas as pd
import numpy as np

project_root = os.path.dirname(os.path.dirname(__file__))


def convert_to_class4(scores):
    k = scores.copy()
    k[k < 2.5] = 0
    k[np.logical_and(k >= 2.5, k < 3.5)] = 1.0
    k[np.logical_and(k >= 3.5, k <= 4.5)] = 2.0
    k[k > 4.5] = 3.0
    return k


def convert_to_class3(scores):
    k = scores.copy()
    k[k < 2.5] = 0
    k[np.logical_and(k >= 2.5, k < 4)] = 1.0
    k[k >= 4] = 2.0
    return k


def convert_to_class2(scores):
    k = scores.copy()
    k[k < 2.5] = 0
    k[k >= 2.5] = 1.0
    return k


def convert_to_pass_fail(scores, thresh=2.5):
    """
    Converts a list of scores to a binary classification.
    Every value < thresh is assigned the value of 0.
    Every value >= thresh is assigned the value of 1.

    Example:

        >>> x = np.array([1,2,3,4,5])
        >>> res = convert_to_pass_fail(x, thresh=2.5)
        >>> print(res)
        [0,0,1,1,1]

    """
    tmp = scores.copy()
    tmp[tmp < thresh] = 0
    tmp[tmp >= thresh] = 1
    return tmp


def min_max_normalization(x):
    mn = np.min(x)
    mx = np.max(x)
    return (x - mn) / (mx - mn)


def create_group_id_vector(df):
    gids = []
    for index, row in df.iterrows():
        gid = '{}.{}'.format(row['AID'], row['QID'])
        gids.append(gid)
    return np.array(gids)


def load_dataframe(language='en', version=1, pass_fail=False):
    global project_root

    if pass_fail:
        filename = 'asag_v{}_{}_pf.csv'.format(version, language)
        filepath = os.path.join(project_root, 'datasets/pass_fail', filename)
        return pd.read_csv(filepath, sep=',', index_col=False)
    else:
        filename = 'asag_v{}_{}.csv'.format(version, language)
        filepath = os.path.join(project_root, 'datasets/normal', filename)
        return pd.read_csv(filepath, sep='~', index_col=False)


def merge_dataframe(df1, df2):
    return pd.concat([df1, df2], axis=1)


def save_dataframe(df, language, version, df_type='feature'):
    dirpath = ''
    if df_type == 'feature':
        dirpath = 'datasets/features'
    elif df_type == 'pass_fail':
        dirpath = 'datasets/pass_fail'
    elif df_type == 'normal':
        dirpath = 'datasets/normal'
    else:
        raise Exception("Invalid dataframe type: {}".format(df_type))

    filename = 'asag_v{}_{}.csv'.format(version, language)
    filepath = os.path.join(project_root, dirpath, filename)
    df.to_csv(filepath, sep='~', index=False)
