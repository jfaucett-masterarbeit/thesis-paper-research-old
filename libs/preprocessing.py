import nltk
from nltk.corpus import stopwords
from nltk.tokenize import WhitespaceTokenizer
from nltk.stem import SnowballStemmer, WordNetLemmatizer
from nltk.corpus import wordnet as wn
import string
import numpy as np
import hunspell

EN_STOPS = stopwords.words('english')
DE_STOPS = stopwords.words('german')
ES_STOPS = stopwords.words('spanish')

EN_STEMMER = SnowballStemmer('english')
DE_STEMMER = SnowballStemmer('german')
ES_STEMMER = SnowballStemmer('spanish')


def create_dictionary(nlp, sents, stops=EN_STOPS):
    results = set()
    for sent in sents:
        k = nlp(sent)
        tokens = [t.lemma_ for t in k if t.dep_ != 'punct' and not t.lemma_ in stops]
        for t in tokens:
            results.add(t)
    return list(results)
    
    

def create_hunspell(os='linux'):
    if os == 'linux':
        return hunspell.HunSpell('/usr/share/hunspell/en_US.dic', '/usr/share/hunspell/en_US.aff')
    else:
        raise Exception('Hunspell for OS: {} has not been implemented.')

def create_spell_autocorrect(hspell, dictionary):
    
    def spell_autocorrect(tokens):
        result = []
        for t in tokens:
            corrected = False
            
            if hspell.spell(t) == False:
                suggestions = hspell.suggest(t)
                for sug in suggestions:
                    if sug in dictionary:
                        result.append(sug)
                        corrected = True
                        break
            if not corrected:
                result.append(t)
        return result
    
    return spell_autocorrect
    
def spacy_lemmatize(tokens):
    result = []
    for t in tokens:
        if t.pos_ == 'PRON':
            result.append(t.text.lower())
        else:
            result.append(t.lemma_)
    return result

def spacy_remove_stopwords(tokens, stopwords):
    result = []
    for t in tokens:
        if t.lemma_ in stopwords:
            continue
        else:
            result.append(t)
    return result

def spacy_punctuation(tokens):
    result = []
    for t in tokens:
        if t.pos_ == 'PUNCT' or t.pos_ == 'SPACE':
            continue
        else:
            result.append(t)
    return result

def bow_tokenize(sentence):
    tok = WhitespaceTokenizer()
    tokens = tok.tokenize(sentence)
    tokens = remove_punctuation(tokens)
    return stem(tokens)


def tokenize(sentence):
    tok = WhitespaceTokenizer()
    return tok.tokenize(sentence)


def clean_word(word):
    return word.replace('.', '').replace('-', '').replace(':', '').replace(';', '').replace(',', '')


def remove_punctuation(tokens):
    return [clean_word(w) for w in tokens if not w in string.punctuation and len(clean_word(w)) > 0]


def lowercase(tokens):
    return [w.lower() for w in tokens]


def create_stopword_remover(stopwords=EN_STOPS):
    def remove_stopwords(tokens):
        return [t for t in tokens if not t.lower() in stopwords]
    return remove_stopwords
    
def remove_stopwords(tokens, stopwords=EN_STOPS):
    return [w for w in tokens if not w in stopwords]


def stem(tokens, stemmer=EN_STEMMER):
    return [stemmer.stem(w) for w in tokens]

def create_spacy_pipeline(nlp, pipeline_map):

    def pipeline(sentence):
        tokens = nlp(sentence)
        result = [t for t in tokens]
        for step, func in pipeline_map.items():
            try:
                result = func(result)
            except Exception as err:
                print("[{}]: failed.".format(step))
                print("[reason]: {}".format(err))

        return result

    return pipeline

def create_pipeline(pipeline_map, tokenizer):

    def pipeline(sentence):
        tokens = tokenizer(sentence)
        result = tokens.copy()
        for step, func in pipeline_map.items():
            try:
                result = func(result)
            except Exception as err:
                print("[{}]: failed.".format(step))
                print("[reason]: {}".format(err))

        return result

    return pipeline

