from nltk.corpus import stopwords
from nltk.corpus import wordnet as wn
from scipy import spatial
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from nltk import ngrams, edit_distance
from nltk.translate.bleu_score import sentence_bleu, SmoothingFunction
import libs.ml as ml
import numpy as n
import pandas as pd
from scipy.stats import entropy
from numpy.linalg import norm
import numpy as np
import libs.text2text as tt

EN_STOPS_RAW = stopwords.words('english')

SF = SmoothingFunction()


def create_preprocess_fn(nlp, pipeline):

    def preprocess_fn(sent):
        tokens = nlp(sent)
        state_map = {}
        for key, value in pipeline.items():
            state_map[key], tokens = value(state_map, tokens)
        return tokens

    return preprocess_fn


#
# https://stackoverflow.com/questions/15880133/jensen-shannon-divergence

def jensen_shannon_divergence(x, y):
    _x = x / norm(x, ord=1)
    _y = y / norm(y, ord=1)
    _M = 0.5 * (_x + _y)
    return 0.5 * (entropy(_x, _M) + entropy(_y, _M))


def cosine_similarity(x, y):
    return 1 - spatial.distance.cosine(x, y)


def jaccard_similarity(x, y):
    return 1 - spatial.distance.jaccard(x, y)


def create_tokenizer(nlp_fn, preprocess_fn):
    def tokenize(sentence):
        return preprocess_fn(nlp_fn(sentence))
    return tokenize


def create_tfidf_vectorizer(tokenizer, ngram_range=(1, 1), **kwargs):
    tfidf = TfidfVectorizer(stop_words=None,
                            tokenizer=tokenizer, ngram_range=ngram_range, **kwargs)
    return tfidf


def create_count_vectorizer(tokenizer, ngram_range=(1, 1)):
    cv = CountVectorizer(stop_words=None,
                         ngram_range=ngram_range, tokenizer=tokenizer)
    return cv


def feature_builder(df, nlp, feature_map, logging=False, log_counts=100):
    features = []
    columns = ['GID'] + list(feature_map.keys())
    for index, row in df.iterrows():
        row_features = []

        # Add row Group ID (Assignment + Question)
        aqid = '{}.{}'.format(row['AID'], row['QID'])
        row_features.append(aqid)

        ma = nlp(row['Model Answer'])
        sa = nlp(row['Student Answer'])

        for feature_key, feature_fn in feature_map.items():
            feature = feature_fn(df, ma, sa, row)
            row_features.append(feature)

        # Add score

        features.append(row_features)

        if logging and (len(features) % log_counts == 0):
            print('Features [{}]: done.'.format(len(features)))
    return pd.DataFrame(features, columns=columns)

# =====================================
# Word-Length Difference Features
# =====================================


def create_feature_length_difference(pos_ignore=['PUNCT', 'SPACE'], stopwords=[]):

    def feature_length_difference(df, ma, sa, row):

        ma_t = [t.lemma_ for t in ma if not t.pos_ in pos_ignore and not t in stopwords]
        sa_t = [t.lemma_ for t in sa if not t.pos_ in pos_ignore and not t in stopwords]

        return sentence_length_difference(ma_t, sa_t)

    return feature_length_difference


# =====================================
# Count Vector Features
# =====================================

def create_feature_vectorizer_distance(vectorizer, distance_fn):

    def feature_vectorizer_distance(df, ma, sa, row):

        row_ma = row['Model Answer']
        row_sa = row['Student Answer']

        return vector_distance_measure(row_ma, row_sa, vectorizer, distance_fn)

    return feature_vectorizer_distance


# =====================================
# N-GRAM Features
# =====================================

def create_feature_ngram_scorer(ngram_fn, pos_ignore=['PUNCT', 'SPACE'], stopwords=[], factor=None):

    def feature_ngram_score(df, ma, sa, row):
        ma_t = [t.lemma_ for t in ma if not t.pos_ in pos_ignore and not t in stopwords]
        sa_t = [t.lemma_ for t in sa if not t.pos_ in pos_ignore and not t in stopwords]

        if factor:
            return ngram_fn(ma_t, sa_t, factor)
        else:
            return ngram_fn(ma_t, sa_t)

    return feature_ngram_score


# =====================================
# WordNet (Knowledge-Base) Features
# =====================================

def create_wordnet_features(sim_fn, lang, stopwords=[], add_lexical_matching=False, open_classes=['NOUN', 'VERB']):

    def wordnet_feature(df, ma, sa, row):

        score = tt.run_entailment_fn(
            ma, sa, sim_fn, lang, stopwords, add_lexical_matching, open_classes)
        return score

    return wordnet_feature


# =====================================
# Vector Space Model Features
# =====================================

def create_feature_wmdistance(model, pos_ignore=['PUNCT', 'SPACE'], stopwords=[]):

    def feature_wmdistance(df, ma, sa, row):
        ma_t = [t.lemma_ for t in ma if not t.pos_ in pos_ignore and not t in stopwords]
        sa_t = [t.lemma_ for t in sa if not t.pos_ in pos_ignore and not t in stopwords]

        dist = model.wmdistance(' '.join(ma_t), ' '.join(sa_t))

        # Cap million
        if dist > 1e6:
            return 1e6
        else:
            return dist

    return feature_wmdistance


def create_feature_vector_cosine():

    def feature_vector_cosine(df, ma, sa, row):
        return ma.similarity(sa)

    return feature_vector_cosine


# =====================================
# Token Based Features
# =====================================

def create_feature_token_based(distance_fn, preprocess_fn):

    def feature_token_based(df, ma, sa, row):
        ma_t = preprocess_fn(ma)
        sa_t = preprocess_fn(sa)
        return distance_fn(ma_t, sa_t)

    return feature_token_based

# =====================================
# Syntax Based Features
# =====================================


def create_feature_pos_edit_distance(token_filter):

    def feature_pos_edit_distance(df, ma, sa, row):
        ma_t = token_filter(ma)
        sa_t = token_filter(sa)
        return pos_edit_distance(ma_t, sa_t)

    return feature_pos_edit_distance


def create_feature_dp_edit_distance(token_filter):

    def feature_dp_edit_distance(df, ma, sa, row):
        ma_t = token_filter(ma)
        sa_t = token_filter(sa)
        return dep_parse_edit_distance(ma_t, sa_t)

    return feature_dp_edit_distance


def create_feature_tree_edit_distance(token_filter):

    def feature_tree_edit_distance(df, ma, sa, row):
        ma_t = token_filter(ma)
        sa_t = token_filter(sa)
        return tree_edit_distance(ma_t, sa_t)

    return feature_tree_edit_distance


def create_feature_bleu_score(preprocess_fn, smoothing_function):

    def feature_bleu_score(df, ma, sa, row):
        ma_t = preprocess_fn(ma)
        sa_t = preprocess_fn(sa)
        return bleu_score(ma_t, sa_t, smoothing_function)

    return feature_bleu_score


# =============
# Features
# =============


def sentence_length_difference(s1, s2):
    """
    Feature: Find difference between the lengths of two sentences.

    Difference in the length of 2 iteratable elements (lists, strings).
    """
    return len(s1) - len(s2)


def vector_distance_measure(ma, sa, vector_model, distance_fn):
    """
    Feature: Find the distance between two vectors.

    vector_model - can be CountVectorizer or TfidfVectorizer
    distance_fn - can be any distance measurement function; for example cosine_similarity() or jaccard_similarity()

    Returns a numeric value in the range [0,1.0]
    """
    result = None
    try:
        mat = vector_model.fit_transform([ma, sa])
        a = mat.toarray()
        result = distance_fn(a[0], a[1])
    except ValueError:
        # silently fail on value errors, this means we tried to transform an empty array (when student entered no answer)
        pass
    if not result or np.isnan(result):
        return 0.0
    else:
        return result


def unigram_score(ma, sa, factor=1):
    ma_s = set(ma)
    sa_s = set(sa)
    hits = len(ma_s.intersection(sa_s))
    total = len(ma_s.union(sa_s))

    if total < 1:
        return 0.0

    return np.power(hits / total, factor)


def bigram_score(ma, sa, factor=1/2):
    ma_b = set(list(ngrams(ma, 2)))
    sa_b = set(list(ngrams(sa, 2)))

    hits = len(ma_b.intersection(sa_b))
    total = len(ma_b.union(sa_b))

    if total < 1:
        return 0.0

    return np.power(hits / total, factor)


def trigram_score(ma, sa, factor=1/3):
    ma_b = set(list(ngrams(ma, 3)))
    sa_b = set(list(ngrams(sa, 3)))

    hits = len(ma_b.intersection(sa_b))
    total = len(ma_b.union(sa_b))

    if total < 1:
        return 0.0

    return np.power(hits / total, factor)


def vsm_wmdistance(model, ma, sa):
    return model.wmdistance(' '.join(ma), ' '.join(sa))


def vsm_entailment_similarity(model, ma, sa):
    """
    Find the word with the maximum similarity
    """
    sims = [0.0]
    for x in ma:
        max_sim = 0.0
        for j in sa:
            tmp = 0.0
            try:
                tmp = model.similarity(x, j)
            except KeyError:
                # one of the tokens does not exists
                pass

            if tmp > max_sim:
                max_sim = tmp
        sims.append(max_sim)
    return np.mean(max_sim)


def basic_edit_distance(ma, sa):
    mx = np.max([len(ma), len(sa)])
    return edit_distance(ma, sa) / (mx + 1e-300)


def tree_edit_distance(ma, sa):
    return ml.tree_distance(ma, sa)


def dep_parse_edit_distance(ma, sa):
    t1 = [t.dep_ for t in ma]
    t2 = [t.dep_ for t in sa]
    mx = np.max([len(t1), len(t2)])
    return edit_distance(t1, t2) / (mx + 1e-300)


def pos_edit_distance(ma, sa):
    t1 = [t.pos_ for t in ma]
    t2 = [t.pos_ for t in sa]
    mx = np.max([len(t1), len(t2)])
    return edit_distance(t1, t2) / (mx + 1e-300)


def bleu_score(ma, sa, smoothing_function=SF.method1):
    return sentence_bleu([ma], sa, weights=[1, 0.3, 0.1], smoothing_function=smoothing_function)


def lexical_dice_coefficient(s1, s2):
    s1 = set(s1)
    s2 = set(s2)
    num = 2 * len(s1.intersection(s2))
    den = len(s1) + len(s2)
    if den > 0:
        return num / den
    else:
        return 0.0


def lexical_jaccard_coefficient(s1, s2):
    s1 = set(s1)
    s2 = set(s2)
    num = len(s1.intersection(s2))
    den = len(s1) + len(s2) - num

    if den > 0:
        return num / den
    else:
        return 0.0


def lexical_cosine_coefficient(s1, s2):
    s1 = set(s1)
    s2 = set(s2)
    num = len(s1.intersection(s2))
    den = np.sqrt(len(s1)) * np.sqrt(len(s2))
    if den > 0:
        return num / den
    else:
        return 0.0


def lexical_faucett_coefficient(s1, s2, relax_ratio=1/2):
    s1 = set(s1)
    s2 = set(s2)
    shared = s1.intersection(s2)
    total = s1.union(s2)

    return np.power(len(shared) / len(total), relax_ratio)
