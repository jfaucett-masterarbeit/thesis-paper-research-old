from scipy import spatial
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
import spacy
import numpy as np


def create_bow_tokenizer(nlp):

    def bow_tokenizer(sentence):
        tokens = []
        for t in nlp(sentence):
            if t.pos_ == 'PUNCT' or t.pos_ == 'SPACE':
                continue
            if t.pos_ == 'PRON':
                tokens.append(t.text.lower())
            else:
                tokens.append(t.lemma_)
        return tokens

    return bow_tokenizer


def cosine_similarity(x, y):
    return 1 - spatial.distance.cosine(x, y)


def create_count_vectorizer(stop_words, tokenizer, ngram_range=(1, 1)):
    cv = CountVectorizer(stop_words=stop_words,
                         ngram_range=ngram_range, tokenizer=tokenizer)
    return cv


def create_tfidf_vectorizer(stop_words, tokenizer, ngram_range=(1, 1)):
    tfidf = TfidfVectorizer(stop_words=stop_words,
                            ngram_range=ngram_range, tokenizer=tokenizer)
    return tfidf


def create_feature_bow_counter(bow_model):

    def feature_bow_counter(df, ma, sa, row):
        score = vector_cosine_distance(
            row['Model Answer'], row['Student Answer'], bow_model)
        return score

    return feature_bow_counter


def vector_cosine_distance(ma, sa, bow_model):
    mat = bow_model.fit_transform([ma, sa])
    arr = mat.toarray()
    pred_coeff = cosine_similarity(arr[0], arr[1])
    if np.isnan(pred_coeff):
        return 0.0
    else:
        return pred_coeff


def round_score(pred_score):
    return int(round(pred_score * 5, 0))


def execute_bow_model(df, vsm_fn):

    # convert the model answer from a 0-5 by 0.5 scale (11 possible scores) to a 0-5 by 1 scale (6 possible scores)
    y_true = list(map(round, df['Score'].tolist()))
    y_true_raw = df['Score'].tolist()
    y_pred = []
    y_pred_raw = []

    model_answers = df['Model Answer'].tolist()
    student_answers = df['Student Answer'].tolist()

    index = 0
    while index < len(model_answers):
        # collect the model and student answers
        model_answer = model_answers[index]
        student_answer = student_answers[index]

        # score the student response
        mat = vsm_fn.fit_transform([model_answer, student_answer])
        a = mat.toarray()
        pred_coeff = cosine_similarity(a[0], a[1])
        if np.isnan(pred_coeff):
            # nan occurs when the student answer is empty due to stopword replacement and preprocessing
            y_pred.append(0.0)
            y_pred_raw.append(0.0)
        else:
            y_pred_score = round_score(pred_coeff)
            y_pred_raw.append(y_pred_score)
            y_pred.append(y_pred_score)

        # continue
        index += 1

    return {
        'y_true': np.array(y_true),
        'y_true_raw': np.array(y_true_raw),
        'y_pred': np.array(y_pred),
        'y_pred_raw': np.array(y_pred_raw)
    }


def get_sentence_distance(vec_model, s1, s2):
    mat = vec_model.fit_transform([s1, s2])
    x = mat.toarray()
    return cosine_similarity(x[0], x[1])


def execute_multiple_bow_models(df, stop_words, tokenizer):
    vec1 = create_count_vectorizer(stop_words, tokenizer, ngram_range=(1, 1))
    vec2 = create_count_vectorizer(stop_words, tokenizer, ngram_range=(2, 2))
    vec3 = create_count_vectorizer(stop_words, tokenizer, ngram_range=(3, 3))

    # convert the model answer from a 0-5 by 0.5 scale (11 possible scores) to a 0-5 by 1 scale (6 possible scores)
    y_true = list(map(round, df['Score'].tolist()))
    y_true_raw = df['Score'].tolist()
    y_pred = []
    y_pred_raw = []

    model_answers = df['Model Answer'].tolist()
    student_answers = df['Student Answer'].tolist()

    index = 0
    while index < len(model_answers):
        # collect the model and student answers
        model_answer = model_answers[index]
        student_answer = student_answers[index]

        # score the student response
        pc1 = get_sentence_distance(vec1, model_answer, student_answer)
        try:
            pc2 = get_sentence_distance(vec2, model_answer, student_answer)
        except ValueError:
            pc2 = 0.0

        try:
            pc3 = get_sentence_distance(vec3, model_answer, student_answer)
        except ValueError:
            pc3 = 0.0

        total_score = 0.0

        power_index = 2
        for k in [pc1, pc2, pc3]:
            if np.isnan(k):
                continue
            total_score += np.power(k, 1/power_index)
            power_index += 1
        # weights = 1,0.5,0.2
        pred_score = np.min([total_score / np.sqrt(3), 1.0])

        y_pred_raw.append(pred_score)
        y_pred.append(round_score(pred_score))

        # continue
        index += 1

    return {
        'y_true': np.array(y_true),
        'y_true_raw': np.array(y_true_raw),
        'y_pred': np.array(y_pred),
        'y_pred_raw': np.array(y_pred_raw)
    }
