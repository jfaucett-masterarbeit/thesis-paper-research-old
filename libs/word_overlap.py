import numpy as np


def execute_word_overlap_model(df, pipeline_fn, overlap_fn):

    # convert the model answer from a 0-5 by 0.5 scale (11 possible scores) to a 0-5 by 1 scale (6 possible scores)
    true_scores = list(map(int, df['Score'].tolist()))
    pred_scores = []

    model_answers = df['Model Answer'].tolist()
    student_answers = df['Student Answer'].tolist()

    index = 0
    while index < len(model_answers):
        # collect the model and student answers
        model_answer = model_answers[index]
        student_answer = student_answers[index]

        # score the student response
        pred_coeff = score_student_answer(
            model_answer, student_answer, pipeline_fn, overlap_fn)
        pred_score = int(round(pred_coeff * 5, 0))
        pred_scores.append(pred_score)

        # continue
        index += 1

    return {
        'y_true': np.array(true_scores),
        'y_pred': np.array(pred_scores)
    }


def score_student_answer(model_answer, student_answer, pipeline_fn, overlap_fn):

    s1 = pipeline_fn(model_answer)
    s2 = pipeline_fn(student_answer)

    ss1 = set(s1)
    ss2 = set(s2)

    if len(ss1) == 0 or len(ss2) == 0:
        return 0.0

    return overlap_fn(ss1, ss2)


def dice_coefficient(s1, s2):
    num = 2 * len(s1.intersection(s2))
    den = len(s1) + len(s2)
    return num / den


def jaccard_coefficient(s1, s2):
    num = len(s1.intersection(s2))
    den = len(s1) + len(s2) - num
    return num / den


def cosine_coefficient(s1, s2):
    num = len(s1.intersection(s2))
    den = np.sqrt(len(s1)) * np.sqrt(len(s2))
    return num / den


def faucett_coefficient(s1, s2, relax_ratio=1/2):
    shared = s1.intersection(s2)
    total = s1.union(s2)

    return np.power(len(shared) / len(total), relax_ratio)
