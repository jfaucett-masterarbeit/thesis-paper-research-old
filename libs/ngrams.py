from nltk import ngrams
import numpy as np
from scipy.stats import hmean


def create_feature(ngram_fn, ngram_size, pos_filter=['PUNCT', 'SPACE'], stopwords=[], factor=None):

    def ngram_feature(df, ma, sa, row):
        ma_t = [t.lemma_ for t in ma if not t.pos_ in pos_filter and not t in stopwords]
        sa_t = [t.lemma_ for t in sa if not t.pos_ in pos_filter and not t in stopwords]

        score = ngram_fn(ma_t, sa_t, ngram_size)
        if factor:
            return np.power(score, factor)
        else:
            return score

    return ngram_feature


def ngram_score(ma, sa, ngram_size):
    evidence_score = evidence_ngram_score(ma, sa, ngram_size)
    confusion_score = confusion_ngram_score(ma, sa, ngram_size)

    if evidence_score > 0 and confusion_score > 0:
        return hmean([evidence_score, confusion_score])
    else:
        return 0.0


def evidence_ngram_score(ma, sa, ngram_size):
    ma_s = set(list(ngrams(ma, ngram_size)))
    sa_s = set(list(ngrams(sa, ngram_size)))

    # how many ngrams were found in SA?
    evidence_ngrams = ma_s.intersection(sa_s)

    # how many ngrams were possible?
    possible_ngrams = len(ma_s)

    if possible_ngrams > 0:
        return len(evidence_ngrams) / possible_ngrams
    else:
        return 0.0


def confusion_ngram_score(ma, sa, ngram_size):
    ma_s = set(list(ngrams(ma, ngram_size)))
    sa_s = set(list(ngrams(sa, ngram_size)))

    # how many ngrams did the SA find in the MA?
    hit_ngrams = sa_s.intersection(ma_s)

    # how many ngrams did the SA attempt?
    attempted_ngrams = len(sa_s)

    if attempted_ngrams > 0:
        return len(hit_ngrams) / attempted_ngrams
    else:
        return 0.0
