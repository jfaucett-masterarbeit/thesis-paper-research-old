import os
from gensim.models import KeyedVectors
from gensim.downloader import base_dir
import libs.preprocessing as pp
import numpy as np
from nltk import ngrams

def load_gensim_model(model_name='glove-wiki-gigaword-300'):
    path = os.path.join(base_dir, model_name, '{}.gz'.format(model_name))
    model = KeyedVectors.load_word2vec_format(path)
    return model

def create_ngrams(tokens, n=2):
    return np.array(list(ngrams(tokens, n))).tolist()


def create_ngram_list(tokens, max_n=2):
    out = []
    for n in range(1, max_n+1):
        ngram_list = create_ngrams(tokens, n)
        out += ngram_list
    return out

def preprocess(sent, stopwords):
    tokens = pp.spacy_remove_stopwords(sent, stopwords)
    tokens = pp.spacy_punctuation(tokens)
    lemmas = pp.spacy_lemmatize(tokens)
    return lemmas

def entailment_similarity(model_answer_ngrams, student_answer_ngrams, sim_fn):
    k = len(model_answer_ngrams)

    total_score = 0

    for model_item in model_answer_ngrams:

        max_item_score = 0

        for student_item in student_answer_ngrams:

            # never compare n-grams of different sizes
            if len(model_item) != len(student_item):
                continue
                
            n = len(model_item)
            item_score = sim_fn(model_item, student_item)
            
            if item_score < 0:
                continue
            
            item_score = np.power(item_score, (1/n))
            
            if item_score > max_item_score:
                max_item_score = item_score

        total_score += max_item_score

    return total_score / k

def create_phrase_similarity_function(model):
    def phrase_vector_similarity(v1, v2):
        n = len(v1)
        sim = 0.0
        try:
            if n == 1:
                sim = model.similarity(v1[0], v2[0])
            else:
                sim = model.n_similarity(v1, v2)
        except KeyError:
            # some word in the sequence is not in the vocabulary.
            pass
        return sim

    return phrase_vector_similarity

def create_word_to_word_similarity_function(model):

    def word_to_word_vector_similarity(v1, v2):
        n = len(v1)
        if n == 0:
            return 0.0
        
        sims = 0
        for i in range(len(v1)):
            x1 = v1[i]
            x2 = v2[i]

            try:
                sims += model.similarity(x1, x2)
            except KeyError:
                # word x1 or x2 is not in vocabulary
                pass

        return sims / n

    return word_to_word_vector_similarity
