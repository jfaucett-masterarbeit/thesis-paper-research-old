import numpy as np
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, confusion_matrix, cohen_kappa_score
import matplotlib.pyplot as plt
import itertools
from cycler import cycler

# Styles
monochrome = (cycler('color', ['k']) * cycler('linestyle',
                                              ['-', '--', ':', '=.']) * cycler('marker', ['^', ',', '.']))
bar_cycle = (cycler('hatch', ['///', '--', '...', '\///', 'xxx',
                              '\\\\']) * cycler('color', 'w')*cycler('zorder', [10]))
styles = bar_cycle()


def covariance(x, y):
    mx = np.mean(x)
    my = np.mean(y)

    return (1/len(x)) * np.sum((x-mx) * (y-my))


def create_random_sequence(values, size):
    return np.random.choice(values, size=size, replace=True)


def create_random_sample(vec):
    n = len(vec)
    values = np.unique(vec)
    return create_random_sequence(values=values, size=n)


def root_mean_square_error(x, y):
    n = len(x)
    return np.sqrt(np.sum(np.power(x - y, 2)) / n)


def mean_absolute_error(x, y):
    n = len(x)
    return np.sum(np.abs(x - y)) / n


def print_big4(y_true, y_pred):
    """
    Calculates precision, accuracy, recall, and F1.
    It uses a weighted score to account for class imbalance.
    """
    print('Accuracy: {:.3f}%'.format(accuracy_score(y_true, y_pred) * 100))
    print('Precision: {:.3f}%'.format(precision_score(
        y_true, y_pred, average='weighted') * 100))
    print('Recall: {:.3f}%'.format(recall_score(
        y_true, y_pred, average='weighted') * 100))
    print('F1-Measure: {:.3f}%'.format(f1_score(y_true,
                                                y_pred, average='weighted') * 100))


def pearsons_r(y_true, y_pred):
    r = covariance(y_true, y_pred) / (np.std(y_true) * np.std(y_pred))
    return r


def show_all_stats(y_true, y_pred):
    print_big4(y_true, y_pred)
    print_stats(y_true, y_pred)
    plot_confusion_matrix(cm(y_true, y_pred), classes=np.unique(y_true))


def print_stats(y_true, y_pred):
    r = pearsons_r(y_true, y_pred)
    print("Pearson's R: {:.2f}".format(r))
    print("Cohen's Kappa: {:.2f}".format(cohen_kappa_score(y_true, y_pred)))
    print("RMSE: {:.2f}".format(root_mean_square_error(y_true, y_pred)))
    print("MAE: {:.2f}".format(mean_absolute_error(y_true, y_pred)))
    print("Y-True Mean: {:.2f}".format(np.mean(y_true)))
    print("Y-Pred Mean: {:.2f}".format(np.mean(y_pred)))
    print("Y-True Std Deviation: {:.2f}".format(np.std(y_true)))
    print("Y-Pred Std Deviation: {:.2f}".format(np.std(y_pred)))


def cm(y_true, y_pred):
    return confusion_matrix(y_true, y_pred)


def plot_confusion_matrix(cm, classes, normalize=False, title='Confusion matrix', cmap='gray', savefile=''):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    thresh = cm.max() / 2.

    plt.imshow(cm, interpolation='nearest', cmap=plt.cm.Greys)
    plt.title(title, size=18)
    # plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45, fontsize=14)
    plt.yticks(tick_marks, classes, fontsize=14)

    fmt = '.2f' if normalize else 'd'

    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black", size=14)

    plt.tight_layout()
    plt.ylabel('True Label', size=16)
    plt.xlabel('Predicted Label', size=16)
    if len(savefile) > 0:
        plt.savefig('{}_cm_bw.png'.format(savefile))
    plt.show()


def plot_histogram_comparison(y_true, y_pred, bins, savefile=''):
    fig, ax = plt.subplots(1, 1)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    ax.set_prop_cycle(monochrome)

    # Grid Setup
    ax.grid(linestyle='--')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)

    # Labels
    ax.set_title('True Labels vs. Predicted Labels Comparison', size=18)
    ax.set(xlabel='Score Values')
    ax.xaxis.label.set_fontsize(16)
    ax.set(ylabel='Number of Scores')
    ax.yaxis.label.set_fontsize(16)

    # Render
    ax.hist(y_true, bins=bins, label='True Labels',
            hatch='//', fill=False, alpha=0.6)
    ax.hist(y_pred, bins=bins, label='Predicted Labels',
            hatch='xxx', fill=False, alpha=0.6)
    plt.legend(loc='upper right')

    # Saving
    if len(savefile) > 0:
        plt.savefig('{}_hist_cmp_bw.png'.format(savefile))
    plt.show()


def plot_scatter(y_true, y_pred):
    plt.title('Raw [0-1] predictions / gold standard [0-5 by 0.5]')
    plt.scatter(y_true, y_pred, alpha=0.5, marker='.')
    plt.xlabel('True Values')
    plt.ylabel("Predicted Values")


def render_histogram(scores, title, xlabel, ylabel, bins, xticks, savefile=''):
    fig, ax = plt.subplots(1, 1)
    plt.xticks(xticks, fontsize=14)
    plt.yticks(fontsize=14)
    ax.set_prop_cycle(monochrome)

    # Grid Setup
    ax.grid(linestyle='--')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)

    # Labels
    ax.set_title(title, size=18)
    ax.set(xlabel=xlabel)
    ax.xaxis.label.set_fontsize(16)
    ax.set(ylabel=ylabel)
    ax.yaxis.label.set_fontsize(16)

    # Render
    ax.hist(scores, bins=bins, hatch='//', fill=False)

    # Saving
    if len(savefile) > 0:
        plt.savefig('{}_hist_bw.png'.format(savefile))
    plt.show()


def render_ecdf(scores, title, xlabel, ylabel, savefile=''):
    fig, ax = plt.subplots(1, 1)
    ax.set_prop_cycle(monochrome)

    # Grid Setup
    ax.grid(linestyle='--')
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)

    # Labels
    ax.set_title(title, size=18)
    ax.set(xlabel=xlabel)
    ax.xaxis.label.set_fontsize(16)
    ax.set(ylabel=ylabel)
    ax.yaxis.label.set_fontsize(16)

    ax.plot(np.sort(scores), np.linspace(0, 1, len(scores), endpoint=False),
            linestyle='-', linewidth=1, marker=',', fillstyle='none')
    if len(savefile) > 0:
        plt.savefig("{}_ecdf_bw.png".format(savefile))
    plt.show()
