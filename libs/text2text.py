import nltk
from nltk.corpus import wordnet as wn
from nltk.corpus.reader.wordnet import WordNetError
import numpy as np

LEACOCK_CHODOROW_MAX_VALUE = 3.6375861597263857
WUP_MAX_VALUE = 0.9285714285714286
JCN_PERFECT_MATCH = 1e+300


def create_jiang_conrath_function(corpus):
    def jcn_similarity(sw1, sw2):
        sim = 0.0
        try:
            sim = wn.jcn_similarity(sw1, sw2, ic=corpus)
        except WordNetError:
            sim = 0.0
        return 1.0 if sim > 1 else sim
    return jcn_similarity


def create_resnik_function(corpus):
    def resnik_similarity(sw1, sw2):
        try:
            max_sim_possible = max(wn.res_similarity(
                sw1, sw1, corpus), wn.res_similarity(sw2, sw2, corpus))
            sim_score = wn.res_similarity(sw1, sw2, corpus)
        except WordNetError:
            sim_score = 0.0
            max_sim_possible = 1.0

        return sim_score / max_sim_possible

    return resnik_similarity


def shortest_path(sense1, sense2):
    return wn.path_similarity(sense1, sense2)


def create_lin_function(corpus):
    def lin_similarity_fn(sw1, sw2):
        sim = 0.0
        try:
            sim = wn.lin_similarity(sw1, sw2, ic=corpus)
        except WordNetError:
            sim = 0.0
        return sim
    return lin_similarity_fn


def leacock_chodorow_similarity(sw1, sw2):
    value = wn.lch_similarity(sw1, sw2)
    if value:
        return value / LEACOCK_CHODOROW_MAX_VALUE
    else:
        return 0.0


def wup_similarity(sw1, sw2):
    value = wn.wup_similarity(sw1, sw2)
    if value:
        return value / WUP_MAX_VALUE
    else:
        return 0.0


def is_open_class(tag, open_classes=['NOUN', 'VERB']):
    if tag in open_classes:
        return True
    return False


def get_pos(tag):
    if tag == 'VERB':
        return wn.VERB
    elif tag == 'NOUN':
        return wn.NOUN
    elif tag == 'ADJ':
        return wn.ADJ
    elif tag == 'ADV':
        return wn.ADV
    else:
        raise Exception('Invalid POS Tag')


def run_entailment_fn(model_answer, student_answer, sim_fn, lang, stop_words, add_lexical_matching=False, open_classes=['NOUN', 'VERB']):
    # Get Open-Class Tokens only
    ma_tokens = [w for w in model_answer if is_open_class(
        w.pos_, open_classes) and not w.lemma_ in stop_words]
    ma_len = len(ma_tokens)

    sa_tokens = [t for t in student_answer if is_open_class(
        t.pos_, open_classes) and not t.lemma_ in stop_words]
    sa_len = len(sa_tokens)

    sim_score = 0.0

    for token in ma_tokens:
        pred_score, best_match = maxsim(
            token, sim_fn, sa_tokens, lang, add_lexical_matching, open_classes)
        sim_score += pred_score

    return sim_score / ma_len


def minsim(student_token, sim_fn, model_response, lang, open_classes):

    # begin by assuming the concept is there.
    min_sim = 1.0
    min_sim_idx = -1
    wn_pos_class = get_pos(student_token.pos_)
    concept_set = wn.synsets(student_token.lemma_, wn_pos_class, lang=lang)

    index = 0
    for model_token in model_response:

        wn_evidence_token_pos = get_pos(model_token.pos_)

        if not is_open_class(model_token.pos_, open_classes):
            index += 1
            continue
        current_sim_min = 1.0

        if wn_pos_class == wn_evidence_token_pos:
            pass

        if current_sim_min < min_sim:
            min_sim_idx = index
            min_sim = current_sim_min

        index += 1

    return (min_sim, min_sim_idx)


def maxsim(token, sim_fn, student_response, lang, add_lexical_matching, open_classes):

    max_sim = 0.0
    wn_pos_class = get_pos(token.pos_)
    concept_set = wn.synsets(token.lemma_, pos=wn_pos_class, lang=lang)

    sim_matches = {}
    max_match = []

    match_indexer = 0

    for evidence_token in student_response:

        evidence_word = evidence_token.text.lower()
        wn_evidence_token_pos = get_pos(evidence_token.pos_)

        if not is_open_class(evidence_token.pos_, open_classes):
            if token.text.lower() == evidence_token.text.lower() and add_lexical_matching:
                max_sim = 1.0
                max_match = [token.text, evidence_token.text]
                break
            continue

        current_sim_max = 0.0

        # Only Compare POS Tags which are the same (VERB=VERB, NOUN=NOUN, etc)
        if wn_pos_class == wn_evidence_token_pos:

            concept_target_set = wn.synsets(
                evidence_token.lemma_, pos=wn_evidence_token_pos, lang=lang)

            for k in concept_set:
                for j in concept_target_set:
                    if k.pos() != j.pos():
                        continue

                    # call the similarity function
                    tmp_sim_value = sim_fn(k, j)

                    if tmp_sim_value:

                        # if our similarity is better than the current best, override
                        if tmp_sim_value > current_sim_max:
                            current_sim_max = tmp_sim_value
                            sim_matches[match_indexer] = [k, j]

            if current_sim_max > 0:
                match_indexer += 1

        if current_sim_max > max_sim:
            max_match = [sim_matches[match_indexer-1]
                         [0], sim_matches[match_indexer-1][1]]
            max_sim = current_sim_max

    return (max_sim, max_match)


def round_score(pred_score):
    return int(round(pred_score * 5, 0))


def run_entailment_model(nlp, df, sim_fn, lang, stop_words, add_lexical_matching=False, open_classes=['NOUN', 'VERB']):

    y_pred_raw = []
    y_pred = []

    counter = 0

    for index, row in df.iterrows():

        ma = nlp(row['Model Answer'])
        sa = nlp(row['Student Answer'])

        entailment_score = run_entailment_fn(
            ma, sa, sim_fn, lang, stop_words, add_lexical_matching, open_classes)

        score = round_score(entailment_score)
        y_pred.append(score)
        y_pred_raw.append(entailment_score)

        counter += 1
        if counter % 300 == 0:
            print('batch: {} done.'.format(counter))

    return {
        'y_true_raw': np.array(df['Score'].tolist()),
        'y_true': np.array(list(map(round, df['Score'].tolist()))),
        'y_pred_raw': np.array(y_pred_raw),
        'y_pred': np.array(y_pred)
    }
