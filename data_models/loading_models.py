from gensim.models import KeyedVectors
from gensim.models import Word2Vec
from gensim.models.wrappers import FastText

def load_word2vec(model_path):
    model = Word2Vec.load(model_path)
    return model

def load_fasttext_saved(model_path):
    model = FastText.load(model_path)
    return model

def load_fastText_bin(model_path):
    model = FastText.load_fasttext_format(model_path)
    return model
