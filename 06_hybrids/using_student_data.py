import pandas as pd
import numpy as np
import nltk
import spacy
from collections import Counter


def show(sents):
    for s in sents:
        print(s)


def prepare(responses):
    sents = [nlp(s) for s in responses]
    result = []
    for sent in sents:
        filtered = [t for t in sent if not t.dep_ in ['punct', 'det'] and not t.is_stop]
        filtered_sent = []
        for item in filtered:
            if item.dep_ == 'poss' or item.pos_ == 'PRON':
                filtered_sent.append(item.text.lower())
            else:
                filtered_sent.append(item.lemma_)
        result.append(filtered_sent)
    return result


def add_ngram(dmap, tkn, idx):
    if tkn in dmap.keys():
        dmap[tkn][0] += 1
        dmap[tkn][1].append(idx)
    else:
        dmap[tkn] = [1, [idx]]


def get_ngrams(prep_sent, ngram_limit=5):
    ngrams = []

    score_idx = 0
    for sent in prep_sent:
        idx = 1
        while idx <= ngram_limit:
            ng = nltk.ngrams(sent, idx)
            ngrams.extend(ng)
            idx += 1
        score_idx += 1

    return ngrams


nlp = spacy.load('en')

df = pd.read_csv('../datasets/normal/asag_v1_en.csv', sep='~')

student_answers = df.iloc[:30]['Student Answer'].tolist()

prep_sents = prepare(student_answers)

results = get_ngrams(prep_sents)

counter = Counter(results)

print(counter.most_common(30))
